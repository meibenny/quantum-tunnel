"""
Throttler should take in some requests
Check if the request interval has passed
If not, block
If yes, make the request
Return the result

Should be thread safe of course
"""

import threading
import queue
import time
import requests

class RequestThrottler:
    def __init__(self):
        self.lock = threading.Lock()
        self.request_interval_in_seconds = 1
        self.time_of_last_request = time.time()

    def get(self, url):
        return self._make_get_request(url)

    def _make_get_request(self, url):
        timeDifference = time.time() - self.time_of_last_request
        while (timeDifference < self.request_interval_in_seconds):
            time.sleep(self.request_interval_in_seconds)
            timeDifference = time.time() - self.time_of_last_request
            print("sleeping for " + str(self.request_interval_in_seconds) + " seconds")
        self.time_of_last_request = time.time()
        return requests.get(url)
