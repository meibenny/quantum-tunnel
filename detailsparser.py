from bs4 import BeautifulSoup
from offense import Offense
import json

class DetailsParser:
    def parse(self, html):
        inmateDetails = []

        htmlSource = BeautifulSoup(html, "html.parser")

        details = htmlSource.find_all("div", class_="details")

        demographics = details[0]
        arrestInformation = details[1]
        offenses = details[2]

        demographics_table = demographics.find("tbody")
        inmateId = self._get_inmateId(demographics_table)
        race = self._get_race(demographics_table)
        address = self._get_address(demographics_table)

        arrestInformation_table = arrestInformation.find("tbody")
        bookingTime = self._get_booking_time(arrestInformation_table)

        offenses_table = offenses.find("tbody")
        offenses = self._get_offenses(offenses_table)
        #print(race)
        #print(address)

        #for eachOffense in offenses:
        #    print(eachOffense.__dict__)

        inmateDetails.append({
            "inmateId": inmateId,
            "race": race,
            "address": address,
            "bookingTime": bookingTime,
            "offenses": offenses
        })
        return inmateDetails

    def _get_inmateId(self, demographics_table):
        idString = str(demographics_table.find_all("tr")[2].find_all("td")[0].string or "")
        inmateId = idString.split(":")[1]
        inmateId = self._sanitize_string(inmateId)
        #print(inmateId)
        return inmateId


    def _get_race(self, demographics_table):
        raceString = str(demographics_table.find_all("tr")[0].find_all("td")[2].contents[2])
        raceString.strip()
        race = raceString.split(":")[1]
        race = self._sanitize_string(race)
        return race

    def _get_address(self, demographics_table):
        address1 = str(demographics_table.find_all("tr")[0].find_all("td")[3].string or "")
        address1 = self._sanitize_string(address1)
        address2 = str(demographics_table.find_all("tr")[1].find_all("td")[2].string or "")
        address2 = self._sanitize_string(address2)
        return address1 + "|" + address2

    def _get_booking_time(self, arrestInformationTable):
        bookingTime = arrestInformationTable.find("tr").find_all("td")[4].string or ""
        bookingTime = self._sanitize_string(bookingTime)
        return bookingTime

    def _get_offenses(self, offenses_table):
        offenses = []
        offensesList = offenses_table.find_all("tr")
        for eachOffense in offensesList:
            offenseAttributes = eachOffense.find_all("td")
            description = self._sanitize_string(str(offenseAttributes[0].string or ""))
            caseNumber = self._sanitize_string(str(offenseAttributes[1].text or ""))
            courtDate = self._sanitize_string(str(offenseAttributes[2].string or ""))
            bondType = self._sanitize_string(str(offenseAttributes[3].string or ""))
            bondAmount = self._sanitize_string(str(offenseAttributes[4].string or ""))
            offenses.append(Offense(description, caseNumber, courtDate, bondType, bondAmount))
        #print(offenses)
        return offenses

    def _sanitize_string(self, string):
        return string.strip().replace("\n", "")