Create a virtual environment:
python -m venv ./venv

Start your virutal environment:
In Windows:
.\venv\Scripts\activate

Install dependencies
pip install -r requirements.txt

Run local web server:  
In Windows:  
set FLASK_APP=app.py  
set FLASK_DEBUG=true  
flask run  

Go to http://localhost:5000/inmates/update