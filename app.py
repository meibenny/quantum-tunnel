from flask import Flask, send_from_directory
from flask_sqlalchemy import SQLAlchemy

from requestthrottler import RequestThrottler
from tulsadownloader import TulsaDownloader
from bookingindexparser import BookingIndexParser
from detailsparser import DetailsParser
from offense import Offense
import datetime
import json

import csv

import re

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Inmate():
    def __init__(self):
        self.lastName = ""
        self.firstName = ""
        self.middleName = ""
        self.address = ""
        self.gender = ""
        self.race = ""
        self.bookingId = ""
        self.bookingDate = ""
        self.bookingTime = ""
        self.dateOfBirth = ""
        self.inmateId = ""
        self.offenses = ""
        self.jail = ""

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def default(obj):
        return obj.__dict__

class InmateModel():
    def __init__(self):
        self.lastname = ""
        self.firstname = ""
        self.middlename = ""
        self.address = ""
        self.gender = ""
        self.race = ""
        self.bookingId = ""
        self.bookingDate = ""
        self.bookingTime = ""
        self.dateOfBirth = ""
        self.inmateId = ""
        self.offenses = ""
        self.jail = ""

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def default(obj):
        return obj.__dict__

# class InmateModel(db.Model):
#     __tablename__ = "inmate"
#     id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
#     lastname = db.Column(db.String())
#     firstname = db.Column(db.String())
#     middlename = db.Column(db.String())
#     address = db.Column(db.String())
#     gender = db.Column(db.String())
#     race = db.Column(db.String())
#     bookingId = db.Column(db.String())
#     bookingDate = db.Column(db.String())
#     bookingTime = db.Column(db.String())
#     dateOfBirth = db.Column(db.String())
#     inmateId = db.Column(db.String())
#     offenses = db.Column(db.String())
#     jail = db.Column(db.String())

#     def __str__(self):
#         return str(self.__dict__)

#     def __repr__(self):
#         return self.__str__()


def downloadInmates():
    throttler = RequestThrottler()
    downloader = TulsaDownloader(throttler)
    parser = BookingIndexParser(downloader)

    parser.get_inmates(downloader.download_first_page())
    num_pages = parser._get_number_of_pages(parser.get_initial_site_state())
    downloader.last_page = num_pages
    # downloader.last_page = 1

    detailsParser = DetailsParser()

    currentDateTime = datetime.datetime.now()

    overview = []
    detailsList = []
    inmates = {}
    for page_num in downloader.get_next_page_number():
        page = downloader.download_page(page_num)
        data = parser.parse(page)
        overview.extend(data)
        links = parser.get_inmate_details_links(page)
        
        dataDate = data[0].get("bookingDate")
        pythonDataDate = datetime.datetime.strptime(dataDate, "%m/%d/%Y")
        print(pythonDataDate)
        if (pythonDataDate.year != currentDateTime.year or pythonDataDate.month != currentDateTime.month or pythonDataDate.day != currentDateTime.day):
            downloader.last_page = page_num
            print("getting yesterday's data")
            print("stop")
        else:
            print(pythonDataDate.year)
            print(pythonDataDate.month)
            print(pythonDataDate.day)

            print(currentDateTime.year)
            print(currentDateTime.month)
            print(currentDateTime.day)


        #print(links)
        for link in links:
            m = re.search(".*DetailsPartial/(\d*).*", link)
            #print(m.group(1))
            details = downloader.download_details(m.group(1))
            detailsList.extend(detailsParser.parse(details))

    for datum in overview:
        inmate = Inmate()
        inmate.inmateId = datum.get("inmateId") or "None"
        inmate.lastName = datum.get("lastName") or "None"
        inmate.firstName = datum.get("firstName") or "None"
        inmate.middleName = datum.get("middleName") or "None"
        inmate.bookingId = datum.get("bookingId") or "None"
        inmate.bookingDate = datum.get("bookingDate") or "None"
        inmate.dateOfBirth = datum.get("dateOfBirth") or "None"
        inmate.gender = datum.get("gender") or "None"
        inmate.jail = "Tulsa"
        inmates[inmate.inmateId] = inmate


    for detail in detailsList:
        inmateId = detail.get("inmateId")
        if (inmateId in inmates):
            inmate = inmates.get(inmateId) or "None"
            inmate.offenses = detail.get("offenses") or "None"
            inmate.address = detail.get("address") or "None"
            inmate.race = detail.get("race") or "None"
            inmate.bookingTime = detail.get("bookingTime") or "None"
            inmates[inmate.inmateId] = inmate or "None"

    inmateList = []
    for k, inmate in inmates.items():
        for offense in inmate.offenses:
            inmateModel = InmateModel()
            inmateModel.lastname = inmate.lastName
            inmateModel.firstname = inmate.firstName
            inmateModel.middlename = inmate.middleName
            inmateModel.address = inmate.address
            inmateModel.gender = inmate.gender
            inmateModel.race = inmate.race
            inmateModel.bookingId = inmate.bookingId
            inmateModel.bookingDate = inmate.bookingDate
            inmateModel.bookingTime = inmate.bookingTime
            inmateModel.dateOfBirth = inmate.dateOfBirth
            inmateModel.inmateId = inmate.inmateId
            inmateModel.offenses = offense
            inmateModel.jail = inmate.jail
            inmateList.append(inmateModel)


    return inmateList

    
# class User(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(80), unique=True, nullable=False)
#     email = db.Column(db.String(120), unique=True, nullable=False)

#     def __str__(self):
#         return str(self.__dict__)

#     def __repr__(self):
#         return self.__str__()

    
# @app.route("/")
# def home():
#     return str(User.query.all())

@app.route("/inmates")
def inmates():
    return str(InmateModel.query.limit(10).all())

@app.route("/inmates/update")
def inmate_updater():
    inmates = downloadInmates()
    # print(inmates)
    # global db
    # db.session.add_all(inmates)
    # db.session.commit()
    columns = [
        "Inmate ID",
        "First Name",
        "Middle Name",
        "Last Name",
        "Address",
        "Gender",
        "Race",
        "Date Of Birth",
        "Booking Time",
        "Booking Date",
        "Offense Description",
        "Case Number",
        "Court Date",
        "Bond Type",
        "Bond Amount",
        "Jail"
    ]

    with open("inmates.csv", "w", newline='') as csvfile:
        inmateWriter = csv.writer(csvfile)
        inmateWriter.writerow(columns)
        for inmate in inmates:
            # for offense in inmate.offenses:
            inmateWriter.writerow([inmate.inmateId,
                                inmate.firstname,
                                inmate.middlename,
                                inmate.lastname,
                                inmate.address,
                                inmate.gender,
                                inmate.race,
                                inmate.dateOfBirth,
                                inmate.bookingTime,
                                inmate.bookingDate,
                                inmate.offenses.description,
                                inmate.offenses.caseNumber,
                                inmate.offenses.courtDate,
                                inmate.offenses.bondType,
                                inmate.offenses.bondAmount,
                                inmate.jail])

    return send_from_directory(".", "inmates.csv", as_attachment=True, attachment_filename="update.csv")