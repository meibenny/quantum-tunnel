class TulsaDownloader:
    def __init__(self, throttler):
        self.throttler = throttler
        self.base_url = "http://iic.tulsacounty.org/expInmateBookings/BookingIndex"
        self.url_args = "?grid-page="
        self.details_url = "http://iic.tulsacounty.org/expInmateBookings/DetailsPartial/"
        self.details_args = "?navid=1"
        self.last_page = None

    def download_first_page(self):
        return self.download_page(1)

    def download_page(self, number):
        # try:
        #     f = open("data/" + str(number) + ".html", "r")
        #     contents = f.read()
        #     f.close()
        #     return contents
        # except:
        #     #print("download page " + str(number))
        #     requestUrl = self.base_url + self.url_args + str(number)
        #     page = self.throttler.get(requestUrl)
        #     page.encoding = "html"
        #     f = open("data/" + str(number) + ".html", "w")
        #     f.write(page.text)
        #     f.close()
        #     return page.text
        requestUrl = self.base_url + self.url_args + str(number)
        page = self.throttler.get(requestUrl)
        page.encoding = "html"
        return page.text
    
    def download_details(self, number):
        # try:
        #     f = open("data/" + str(number) + ".html", "r")
        #     contents = f.read()
        #     f.close()
        #     return contents
        # except:
        #     #print("download page " + str(number))
        #     requestUrl = self.details_url + str(number) + self.details_args
        #     page = self.throttler.get(requestUrl)
        #     page.encoding = "html"
        #     f = open("data/" + str(number) + ".html", "w")
        #     f.write(page.text)
        #     f.close()
        #     return page.text

        requestUrl = self.details_url + str(number) + self.details_args
        page = self.throttler.get(requestUrl)
        page.encoding = "html"
        return page.text

    def get_next_page_number(self):
        num = 1
        while num <= self.last_page:
            yield num
            num += 1
