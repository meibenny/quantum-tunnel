import json

class Offense(json.JSONEncoder):
    def __init__(self, description, caseNumber, courtDate, bondType, bondAmount):
        self.description = description
        self.caseNumber = caseNumber
        self.courtDate = courtDate
        self.bondType = bondType
        self.bondAmount = bondAmount

    def default(obj):
        return obj.__dict__

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()