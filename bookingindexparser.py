from bs4 import BeautifulSoup

class BookingIndexParser:
    def __init__(self, downloader):
        self.downloader = downloader

    def get_initial_site_state(self):
        firstPage = self.downloader.download_first_page()
        return firstPage

    def _get_number_of_pages(self, html):
        htmlSource = BeautifulSoup(html, "html.parser")
        lastPageLink = htmlSource.find_all("ul", class_="pagination")[0].find_all("li")[-2].find("a").get("href")
        lastPage = lastPageLink.split("=")[-1]
        return int(lastPage)
    
    def update_downloader_initial_state(self):
        self.downloader.last_page = self._get_number_of_pages(self.get_initial_site_state())

    def get_inmates(self, bookingpage):
        #print(bookingpage)
        pass

    def _get_details_link(self, inmate_details):
        return inmate_details[0].find("b").find("a").get("href")

    def _get_last_name(self, inmate_details):
        return str(inmate_details[1].string)

    def _get_first_name(self, inmate_details):
        return str(inmate_details[2].string)

    def _get_middle_name(self, inmate_details):
        return str(inmate_details[3].string or "")
    
    def _get_booking_id(self, inmate_details):
        return str(inmate_details[4].string)

    def _get_booking_date(self, inmate_details):
        return str(inmate_details[5].string)

    def _get_date_of_birth(self, inmate_details):
        return str(inmate_details[6].string)

    def _get_gender(self, inmate_details):
        return str(inmate_details[7].string)

    def _get_inmate_id(self, inmate_details):
        return str(inmate_details[8].string)

    def parse(self, html):

        inmateDetails = []

        htmlSource = BeautifulSoup(html, "html.parser")
        inmate_table = htmlSource.tbody
        inmate_rows = inmate_table.find_all("tr")
        for row in inmate_rows:
            inmate_details = row.find_all("td")
            detailsLink = self._get_details_link(inmate_details)
            lastName = self._get_last_name(inmate_details)
            firstName = self._get_first_name(inmate_details)
            middleName = self._get_middle_name(inmate_details)
            bookingId = self._get_booking_id(inmate_details)
            bookingDate = self._get_booking_date(inmate_details)
            dateOfBirth = self._get_date_of_birth(inmate_details)
            gender = self._get_gender(inmate_details)
            inmateId = self._get_inmate_id(inmate_details)

            inmateDetails.append({
                "detailsLink": detailsLink,
                "lastName": lastName,
                "firstName": firstName,
                "middleName": middleName,
                "bookingId": bookingId,
                "bookingDate": bookingDate,
                "dateOfBirth": dateOfBirth,
                "gender": gender,
                "inmateId": inmateId
            })

        return inmateDetails

    def get_inmate_details_links(self, html):
        links = []
        htmlSource = BeautifulSoup(html, "html.parser")
        inmate_table = htmlSource.tbody
        inmate_rows = inmate_table.find_all("tr")
        for row in inmate_rows:
            inmate_details = row.find_all("td")
            inmate_detail_data = inmate_details[0]
            inmate_detail_link = inmate_detail_data.find("b").find("a").get("href")
            links.append(inmate_detail_link)
        return links

    def get_inmate_name(self, html):
        htmlSource = BeautifulSoup(html, "html.parser")


